package edu.campus02.cocktails.test;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.campus02.cocktails.Getraenk;

public class GetraenkeTest {
	@Test
	public void construktor01() throws Exception {
		Getraenk g = new Getraenk("Wasser", 200);
		assertEquals("Wasser", g.getName());
		assertEquals(200, g.volumen(), 0.01);
		assertEquals(0, g.alkohol(), 0.01);

	}

	@Test
	public void construktor02() throws Exception {
		Getraenk g = new Getraenk("Bier", 200, 2);
		assertEquals("Bier", g.getName());
		assertEquals(200, g.volumen(), 0.01);
		assertEquals(4, g.alkohol(), 0.01);
	}

	@Test
	public void testToString() throws Exception {
		Getraenk g = new Getraenk("Bier", 200, 2);

		assertEquals("(Bier 2,0% 200,0)", g.toString());
		
	}
}
