package edu.campus02.reise.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.campus02.reise.Etappe;
import edu.campus02.reise.Reise;

public class ReiseTest {
	@Test
	public void reisen01() throws Exception {

		Reise reise01 = new Reise();

		reise01.reisen(new Etappe("Auto", 20));
		reise01.reisen(new Etappe("Zug", 220));
		reise01.reisen(new Etappe("Flugzeug", 1500));

		assertEquals("[(Auto 20,0), (Zug 220,0), (Flugzeug 1500,0)]", reise01.toString());
	}

	@Test
	public void kuerzeste01() throws Exception {

		Reise reise01 = new Reise();

		reise01.reisen(new Etappe("Auto", 250));
		reise01.reisen(new Etappe("AUto", 230));
		reise01.reisen(new Etappe("Auto", 210));
		reise01.reisen(new Etappe("Zug", 220));

		Etappe kürzesteEtappe = reise01.kuerzesteEtappe();

		assertEquals(210, kürzesteEtappe.getEntfernung(), 0.1);
	}

	@Test
	public void kuerzeste02() throws Exception {

		Reise reise01 = new Reise();

		reise01.reisen(new Etappe("Flugzeug", 1500));
		reise01.reisen(new Etappe("Auto", 230));
		reise01.reisen(new Etappe("Auto", 210));
		reise01.reisen(new Etappe("Zug", 220));

		Etappe min = reise01.kuerzesteEtappe();

		assertEquals(210, min.getEntfernung(), 0.1);
	}

	@Test
	public void kuerzeste03() throws Exception {

		Reise reise01 = new Reise();
		reise01.reisen(new Etappe("Banane", 230));
		reise01.reisen(new Etappe("Auto", 210));
		reise01.reisen(new Etappe("Zug", 220));
		reise01.reisen(new Etappe("Flugzeug", 1500));

		Etappe etappe = reise01.kuerzesteEtappe();

		assertEquals(210, etappe.getEntfernung(), 0.1);
	}

	@Test
	public void kuerzeste04() throws Exception {

		Reise reise01 = new Reise();
		reise01.reisen(new Etappe("Fahhrad", 30));
		reise01.reisen(new Etappe("Auto", 210));
		reise01.reisen(new Etappe("Auto", 150));
		reise01.reisen(new Etappe("Zug", 20));

		Etappe kuerzeste = reise01.kuerzesteEtappe("Auto");
		assertEquals(150, kuerzeste.getEntfernung(), 0.1);
	}
	@Test
	public void kuerzeste05() throws Exception {

		Reise reise01 = new Reise();
		reise01.reisen(new Etappe("Zug", 20));
		reise01.reisen(new Etappe("Fahhrad", 30));
		reise01.reisen(new Etappe("Auto", 210));
		reise01.reisen(new Etappe("Auto", 150));

		Etappe kuerzeste = reise01.kuerzesteEtappe("Auto");
		assertEquals(150, kuerzeste.getEntfernung(), 0.1);
	}
	
	
	@Test
	public void zaehle01() throws Exception {

		Reise reise = new Reise();
		reise.reisen(new Etappe("Zug", 230));
		reise.reisen(new Etappe("Auto", 210));
		reise.reisen(new Etappe("Zug", 220));
		reise.reisen(new Etappe("Flugzeug", 1500));

		assertEquals(2, reise.anzahlEtappen("Zug"));
	}

	@Test
	public void zaehle02() throws Exception {

		Reise reise01 = new Reise();
		reise01.reisen(new Etappe("Zug", 230));
		reise01.reisen(new Etappe("Auto", 210));
		reise01.reisen(new Etappe("Zug", 220));
		reise01.reisen(new Etappe("Flugzeug", 1500));

		assertEquals(0, reise01.anzahlEtappen("Fahrrad"));
	}

	@Test
	public void zaehleTransportmittel01() throws Exception {

		Reise reise01 = new Reise();
		reise01.reisen(new Etappe("Zug", 230));
		reise01.reisen(new Etappe("Auto", 210));
		reise01.reisen(new Etappe("Zug", 220));
		reise01.reisen(new Etappe("Flugzeug", 1500));

		assertEquals(3, reise01.zaehleTransportmittel());
	}

	@Test
	public void zaehleTransportmittel02() throws Exception {

		Reise reise01 = new Reise();
		

		assertEquals(0, reise01.zaehleTransportmittel());
	}

	
}
