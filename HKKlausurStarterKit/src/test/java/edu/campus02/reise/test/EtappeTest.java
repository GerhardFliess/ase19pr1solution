package edu.campus02.reise.test;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.campus02.reise.Etappe;

public class EtappeTest {

	
	@Test
	public void constructorTest() throws Exception {
		Etappe test = new Etappe("Auto", 300);
		
		assertEquals(300, test.getEntfernung(),0.01);
		assertEquals("Auto", test.getTransportmittel());
	}

	@Test
	public void toStringTest() throws Exception {
		Etappe test = new Etappe("Auto", 300);
		
		assertEquals("(Auto 300,0)", test.toString());
	}

}
