package edu.campus02.automat;

import static org.junit.Assert.*;

import org.junit.Test;

public class ValidationTest {
	@Test
	public void test01Valid() throws Exception {

		assertTrue(ValidateUtil.verify(CreationUtil.create01(), "aac"));
		assertTrue(ValidateUtil.verify(CreationUtil.create01(), "ac"));
		assertTrue(ValidateUtil.verify(CreationUtil.create01(), "bc"));

	}

	@Test
	public void test01InvalidNoTransition() throws Exception {

		assertFalse(ValidateUtil.verify(CreationUtil.create01(), "ad"));
	}

	@Test
	public void test01InvalidNotFinal() throws Exception {

		assertFalse(ValidateUtil.verify(CreationUtil.create01(), "aa"));
	}

	@Test
	public void test02InvalidNotFinal() throws Exception {

		assertFalse(ValidateUtil.verify(CreationUtil.create02(), "ab"));
	}

	@Test
	public void test02Final() throws Exception {

		assertTrue(ValidateUtil.verify(CreationUtil.create02(), "abc"));
		assertTrue(ValidateUtil.verify(CreationUtil.create02(), "aabbbc"));
	}

	@Test
	public void test02InvalidNotTransition() throws Exception {

		assertFalse(ValidateUtil.verify(CreationUtil.create02(), "ax"));
	}

}
