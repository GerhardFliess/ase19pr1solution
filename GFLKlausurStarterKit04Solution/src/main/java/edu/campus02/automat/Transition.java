package edu.campus02.automat;

public class Transition {

	private String trigger;
	private State target;

	public Transition(String trigger, State target) {
		this.trigger = trigger;
		this.target = target;
	}

	public State getTarget() {
		return target;
	}

	public boolean match(char c) {
		return trigger.contains("" + c);
	}

	public String toString() {
		return String.format("[%s]->%s", trigger, target);
	}

}
