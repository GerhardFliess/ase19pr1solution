package edu.campus02.automat;

public class ValidateUtil {

	public static boolean verify(State start, String word) {

		State currentState = start;
		for (int index = 0; index < word.length(); index++) {
			char charAt = word.charAt(index);
			Transition transition = currentState.findMatchingTransition(charAt);
			if (transition == null) {
				return false;
			} else {
				currentState = transition.getTarget();
			}
		}
		if (currentState.isFinal())
			return true;

		return false;
	}

	public static boolean verifyRekursive(State state, String word) {

		if (word.isEmpty())
			return false;

		char ersterBuchstabe = word.charAt(0);
		Transition findMatchingTransition = state.findMatchingTransition(ersterBuchstabe);
		
		if (findMatchingTransition == null)
			return false;

		State currentState = findMatchingTransition.getTarget();

		if (word.length() == 1) {
			return currentState.isFinal();
		} else {
			String restVomWord = word.substring(1);
			return verifyRekursive(currentState, restVomWord);
		}
	}

}
