package edu.campus02.automat;

import java.util.ArrayList;

public class State {

	private String name;
	private boolean fina;
	private ArrayList<Transition> transitions = new ArrayList<Transition>();

	public State(String name) {
		this.name = name;
	}

	public State(String name, boolean fina) {
		this.name = name;
		this.fina = fina;
	}

	public boolean isFinal() {
		return fina;
	}

	public void addTransition(Transition tran) {
		transitions.add(tran);
	}

	public Transition findMatchingTransition(char c) {
		for (Transition transition : transitions) {
			if (transition.match(c))
				return transition;
		}
		return null;
	}

	public String toString() {
		if (transitions.isEmpty()) {
			return String.format("(%s,%b)", name, isFinal());
		} else {
			return String.format("(%s,%b,%s)", name, isFinal(), transitions);

		}
	}
}
