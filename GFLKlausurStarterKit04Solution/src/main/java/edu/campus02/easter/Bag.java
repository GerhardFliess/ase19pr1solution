package edu.campus02.easter;

import java.util.ArrayList;
import java.util.HashSet;

public class Bag {

	private ArrayList<Egg> eggs = new ArrayList<Egg>();

	public void putEgg(Egg egg) {
		eggs.add(egg);
	}

	public int countEggs() {
		return eggs.size();
	}

	public double sumWeight() {
		double sum = 0;
		for (Egg egg : eggs) {
			sum += egg.getWeight();
		}
		return sum;
	}

	public double sumWeight(String color) {
		double sum = 0;
		for (Egg egg : eggs) {
			if (egg.getColor() == color) {
				sum += egg.getWeight();
			}
		}
		return sum;

	}

	public int countDifferentColors() {
		HashSet<String> colors = new HashSet<String>();
		for (Egg egg : eggs) {
			colors.add(egg.getColor());
		}
		return colors.size();
	}

}
