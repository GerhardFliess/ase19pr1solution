package edu.campus02.easter;

public class Egg {

	private double weigth;
	private String color;

	public Egg(double weight) {
		this.weigth = weight;
		this.color = "weiss";
	}

	public Egg(double weight, String color) {
		this.weigth = weight;
		this.color = color;
	}

	public double getWeight() {
		return weigth;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String toString() {
		return String.format("(%.2f %s)", weigth, color);
	}
}
