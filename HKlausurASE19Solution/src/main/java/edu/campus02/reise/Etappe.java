package edu.campus02.reise;

public class Etappe {

	private String transportmittel;
	private double entfernung;

	public Etappe(String transportmittel, double entfernung) {
		this.transportmittel = transportmittel;
		this.entfernung = entfernung;
	}

	public double getEntfernung() {
		return entfernung;
	}

	public String getTransportmittel() {
		return transportmittel;
	}

	public String toString() {
		return String.format("(%s %.1f)", transportmittel, entfernung);
	}

}
