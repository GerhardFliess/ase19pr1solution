package edu.campus02.reise;

import java.util.ArrayList;
import java.util.HashSet;

public class Reise {

	private ArrayList<Etappe> etappen = new ArrayList<Etappe>();

	public void reisen(Etappe f) {
		etappen.add(f);
	}

	public int anzahlEtappen(String sorte) {

		int anzahl = 0;
		for (Etappe frucht : etappen) {
			if (frucht.getTransportmittel() == sorte)
				anzahl++;
		}
		return anzahl;
	}

	public Etappe kuerzesteEtappe() {
		if (etappen.isEmpty())
			return null;

		Etappe min = etappen.get(0);
		for (Etappe temp : etappen) {
			if (temp.getEntfernung() < min.getEntfernung())
				min = temp;
		}

		return min;
	}

	public ArrayList<Etappe> etappenMitTransportmittel(String transport) {
		ArrayList<Etappe> result = new ArrayList<Etappe>();
		if (transport == null)
			return result;

		for (Etappe etappe : etappen) {
			if (etappe.getTransportmittel().contentEquals(transport))
				result.add(etappe);
		}

		return result;

	}

	public Etappe kuerzesteEtappe(String transportmittel) {
		if (etappen.isEmpty())
			return null;

		ArrayList<Etappe> etappenMitTransportmittel = etappenMitTransportmittel(transportmittel);
		
		if (etappenMitTransportmittel.isEmpty())
			return null;
		
		Etappe min = etappenMitTransportmittel.get(0);

		for (Etappe temp : etappenMitTransportmittel) {
			if (temp.getEntfernung() < min.getEntfernung())
				min = temp;
		}

		return min;
	}

	public int zaehleTransportmittel() {
		HashSet<String> transportmittel = new HashSet<String>();
		for (Etappe etappe : etappen) {
			transportmittel.add(etappe.getTransportmittel());
		}
		return transportmittel.size();
	}

	public String toString() {
		return etappen.toString();
	}
}
