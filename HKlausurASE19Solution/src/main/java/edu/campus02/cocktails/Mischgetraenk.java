package edu.campus02.cocktails;

import java.util.ArrayList;

public class Mischgetraenk {

	private String name;
	private ArrayList<Getraenk> getraenke = new ArrayList<Getraenk>();
	private ArrayList<Mischgetraenk> mischgetraenke = new ArrayList<Mischgetraenk>();

	public Mischgetraenk(String name) {
		this.name = name;
	}

	public void addGetraenk(Getraenk getr) {
		getraenke.add(getr);
	}

	public void addGetraenk(Mischgetraenk getr) {
		mischgetraenke.add(getr);
	}

	public double alkohol() {
		double result = 0;
		for (Getraenk getraenk : getraenke) {
			result += getraenk.alkohol();
		}
		for (Mischgetraenk mgetraenk : mischgetraenke) {
			result += mgetraenk.alkohol();
		}

		return result;
	}

	public double volumen() {
		double result = 0;
		for (Getraenk getraenk : getraenke) {
			result += getraenk.volumen();
		}
		for (Mischgetraenk mgetraenk : mischgetraenke) {
			result += mgetraenk.volumen();
		}
		
		return result;
	}

	public String toString() {

		String result = name;

		if (!getraenke.isEmpty())
			result = String.format("%s %s", result, getraenke);
		
		if (!mischgetraenke.isEmpty())
			result = String.format("%s %s", result, mischgetraenke);
		
		return result;
	}
}
