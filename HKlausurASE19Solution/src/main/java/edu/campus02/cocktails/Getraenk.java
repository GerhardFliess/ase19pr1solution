package edu.campus02.cocktails;

public class Getraenk {

	private String name;
	private double alkoholgehalt = 0;
	private double volumen;

	public Getraenk(String name, double volumen, double alkoholgehaltInProzent) {
		this.name = name;
		this.volumen = volumen;
		this.alkoholgehalt = alkoholgehaltInProzent;
	}

	public Getraenk(String name, double volumen) {
		this.name = name;
		this.volumen = volumen;
	}

	public double volumen() {
		return volumen;
	}

	public double alkohol() {
		return volumen * alkoholgehalt / 100;
	}

	public String getName() {

		return name;
	}

	public String toString() {
		return String.format("(%s %.1f%% %.1f)", name, alkoholgehalt, volumen);
	}
}
