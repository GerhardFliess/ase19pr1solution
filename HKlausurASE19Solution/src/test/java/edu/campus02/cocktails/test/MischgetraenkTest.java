package edu.campus02.cocktails.test;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.campus02.cocktails.Getraenk;
import edu.campus02.cocktails.Mischgetraenk;

public class MischgetraenkTest {

	@Test
	public void add01() throws Exception {
		Mischgetraenk mg = createColaRum();

		assertEquals(204, mg.volumen(), 0.1);
		assertEquals(1.68, mg.alkohol(), 0.1);
	}

	@Test
	public void testToString01() throws Exception {
		Mischgetraenk mg = new Mischgetraenk("Cola-Rum");

		assertEquals("Cola-Rum", mg.toString());
	}

	@Test
	public void testToString02() throws Exception {
		Mischgetraenk mg = createColaRum();

		assertEquals("Cola-Rum [(Cola 0,0% 200,0), (Rum 42,0% 4,0)]", mg.toString());
	}

	@Test
	public void testToString03() throws Exception {

		Mischgetraenk mischMasch = createMischGetränk();

		assertEquals("MischMasch [(Wasser 0,0% 100,0), (Bier 3,2% 250,0)] [Misc [Cola-Rum [(Cola 0,0% 200,0), (Rum 42,0% 4,0)], Gin-Tonic [(Tonic 0,0% 200,0), (Gin 40,0% 6,0)]]]", mischMasch.toString());
	}
	
	@Test
	public void mischAdd01() throws Exception {

		Mischgetraenk mischMasch = createMischGetränk();
		
		assertEquals(760, mischMasch.volumen(),0.1);

	}
	
	@Test
	public void mischAdd02() throws Exception {

		Mischgetraenk mischMasch = createMischGetränk();
		
		assertEquals(12.08, mischMasch.alkohol(),0.1);

	}

	

	private Mischgetraenk createMischGetränk() {
		Mischgetraenk mischMasch = new Mischgetraenk("MischMasch");
		mischMasch.addGetraenk(new Getraenk("Wasser", 100));
		mischMasch.addGetraenk(new Getraenk("Bier", 250, 3.2));

		Mischgetraenk colaRum = createColaRum();

		Mischgetraenk ginTonic = createGinTonic();

		Mischgetraenk misch = new Mischgetraenk("Misc");
		misch.addGetraenk(colaRum);
		misch.addGetraenk(ginTonic);

		mischMasch.addGetraenk(misch);
		return mischMasch;
	}

	private Mischgetraenk createGinTonic() {
		Mischgetraenk ginTonic = new Mischgetraenk("Gin-Tonic");
		ginTonic.addGetraenk(new Getraenk("Tonic", 200));
		ginTonic.addGetraenk(new Getraenk("Gin", 6, 40));
		return ginTonic;
	}

	private Mischgetraenk createColaRum() {
		Mischgetraenk colaRum = new Mischgetraenk("Cola-Rum");
		colaRum.addGetraenk(new Getraenk("Cola", 200));
		colaRum.addGetraenk(new Getraenk("Rum", 4, 42));
		return colaRum;
	}

}
