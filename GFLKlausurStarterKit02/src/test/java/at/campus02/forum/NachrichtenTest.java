package at.campus02.forum;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import at.campus02.ase.nachrichten.Nachrichten;

public class NachrichtenTest {
	@Test
	public void konstruktor() throws Exception {

		Nachrichten eintrag = new Nachrichten("Testtitel", "Testtext");

		assertEquals("(Testtitel,Testtext)", eintrag.toString());
	}

	@Test
	public void antworten() throws Exception {

		Nachrichten eintrag = new Nachrichten("Testtitel", "Testtext");
		Nachrichten a1 = eintrag.antworten("1", "text1");
		assertNotNull(a1);

		Nachrichten a2 = eintrag.antworten("2", "text2");
		assertNotNull(a2);

		ArrayList<Nachrichten> antworten = eintrag.getAntworten();
		assertEquals(2, antworten.size());
	}

	@Test
	public void countMessagesTest() throws Exception {

		Nachrichten eintrag = new Nachrichten("Titel", "Text");
		Nachrichten a1 = eintrag.antworten("1", "Hansi1");
		assertNotNull(a1);

		Nachrichten a2 = eintrag.antworten("2", "Hansi2");
		assertNotNull(a2);

		a1.antworten("11", "11 huhu");
		a1.antworten("12", "12 huhu");

		a2.antworten("21", "21 text");
		a2.antworten("22", "22 text");

		assertEquals(6, eintrag.antwortenZaehlen());
		
	}

	@Test
	public void antwortenString() throws Exception {

		Nachrichten eintrag = new Nachrichten("Testtitel", "Testtext");
		Nachrichten a1 = eintrag.antworten("1", "1 text");
		assertNotNull(a1);

		Nachrichten a2 = eintrag.antworten("2", "2 text");
		assertNotNull(a2);

		a1.antworten("11", "11 text");
		a1.antworten("12", "12 text");
		assertEquals("(1,1 text)[(11,11 text), (12,12 text)]", a1.toString());

		a2.antworten("21", "21 text");
		Nachrichten a22 = a2.antworten("22", "22 text");
		a22.antworten("221", "221 text");

		assertEquals(
				"(Testtitel,Testtext)[(1,1 text)[(11,11 text), (12,12 text)], (2,2 text)[(21,21 text), (22,22 text)[(221,221 text)]]]",
				eintrag.toString());
	}
}
