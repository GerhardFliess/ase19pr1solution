package at.campus02.reise;

import static org.junit.Assert.*;

import org.junit.Test;

import at.campus02.ase.reise.Urlaubsreise;

public class UrlaubsreiseTest {

	@Test
	public void testConstr01() throws Exception {
		Urlaubsreise reise = new Urlaubsreise("Graz");

		assertEquals("Graz", reise.toString());
	}

	@Test
	public void testToString() throws Exception {
		Urlaubsreise reise = new Urlaubsreise("Graz");
		reise.addEtappe("Auto", 200, "Wien");
		reise.addEtappe("Flug", 2000, "Athen");
		
		assertEquals("[Graz -> Wien (200, Auto), Wien -> Athen (2000, Flug)]", reise.toString());
	}

	@Test
	public void testDistance01() throws Exception {
		Urlaubsreise reise = new Urlaubsreise("Graz");
		reise.addEtappe("Auto", 200, "Wien");
		reise.addEtappe("Flug", 2000, "Athen");
		
		assertEquals(2200, reise.ganzeDistanz());
	}

	@Test
	public void testDistance02() throws Exception {
		Urlaubsreise reise = new Urlaubsreise("Graz");
		reise.addEtappe("Auto", 200, "Wien");
		reise.addEtappe("Flug", 2000, "Athen");
		reise.addEtappe("Auto", 20, "Hafen");
			
		assertEquals(220, reise.zurueckgelegtMit("Auto"));
	}
	
	
	@Test
	public void testOneWay01() throws Exception {
		Urlaubsreise reise = new Urlaubsreise("Graz");
		reise.addEtappe("Auto", 200, "Wien");
		reise.addEtappe("Flug", 2000, "Athen");
		reise.addEtappe("Auto", 20, "Hafen");
		assertFalse(reise.isOneWay());
		
	}

	@Test
	public void testOneWay02() throws Exception {
		Urlaubsreise reise = new Urlaubsreise("Graz");
		reise.addEtappe("Auto", 200, "Wien");
		reise.addEtappe("Auto", 300, "Linz");
		reise.addEtappe("Auto", 300, "Graz");
		assertTrue(reise.isOneWay());	
	}
	
	@Test
	public void testOneWay03() throws Exception {
		Urlaubsreise reise = new Urlaubsreise("Graz");
		assertTrue(reise.isOneWay());
		
	}


}