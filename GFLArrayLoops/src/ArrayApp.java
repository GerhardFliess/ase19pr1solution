
public class ArrayApp {
	public static void main(String[] args) {
		int[] feld = new int[] { 2, 4, 5, 7 };

		int sum = 0;
		for (int wert : feld) {
			sum = sum + wert;
		}

		System.out.println(sum);
		for (int wert : feld) {
			System.out.printf(" %d", wert * 3);
		}
		System.out.println();

		int[] mulresult = mul(feld, 3);
		print(mulresult);

		print(addNext(new int[] { 2, 4, 2, 5, 6, 9 }));

	}

	public static int[] mul(int[] array, int mul) {
		int[] result = new int[array.length];
		for (int index = 0; index < array.length; index++) {
			result[index] = array[index] * mul;
		}
		return result;
	}

	public static int[] addNext(int[] array) {
		
		int[] result = new int[array.length / 2];
		for (int index = 0; index < result.length; index++) { 
			result[index] = array[index * 2] + array[index * 2 + 1];
		}
		return result;
	}

	//Hilfsmehtode um das array auszugeben
	public static void print(int[] feld) {
		System.out.print("[");
		for (int wert : feld) {
			System.out.printf(" %d", wert);
		}
		System.out.println(" ]");
	}
}
