
public class ArraysApp {

	public static void main(String[] args) {
		int[] feld = new int[] { 8, 4, 5, 7, 3, 4, 5 };
		int[] feld1 = new int[] { 2, 4, 5, 6, 7, 8, 9 };

		System.out.println(mittel(feld));

		System.out.println(min(feld));

		System.out.println(max(feld));

		System.out.println(doppelte(feld));

		System.out.println(doppelte(feld1));

		print(sort(feld));
		System.out.println();
		print(sort(feld1));
	}

	public static double mittel(int[] array) {
		double result = 0;

		for (int wert : array) {
			result = result + wert;
		}
		return result / array.length;
	}

	public static int min(int[] array) {
		int min = array[0];

		for (int wert : array) {
			if (min < wert)
				min = wert;
		}
		return min;
	}

	public static int max(int[] array) {
		int max = array[0];

		for (int wert : array) {
			if (max > wert)
				max = wert;
		}
		return max;
	}

	public static boolean doppelte(int[] array) {

		for (int suchindex = 0; suchindex < array.length; suchindex++) {
			int suche = array[suchindex];
			for (int innererIndex = suchindex + 1; innererIndex < array.length; innererIndex++) {
				if (array[innererIndex] == suche) {
					System.out.printf("%d kommt zumindest zweimal vor\n", suche);
					return true;
				}
			}
		}
		System.out.println("keine zahl kommt doppelt for");
		return false;
	}

	public static int[] sort(int[] array) {

		boolean vertausche = true;

		while (vertausche) { // solange vertauscht wird
			vertausche = false;

			int currentMax = array[0]; // startwert ist erestes element im feld

			for (int index = 0; index < array.length - 1; index++) { // bis eines vor dem ende suchen
				if (currentMax > array[index + 1]) { // mit nachbar vergleichen
					int temp = array[index + 1]; // wenn nachbar gr��er ist positionen tauschen
					array[index + 1] = currentMax;

					array[index] = temp;
					vertausche = true; // es wurde vertauscht
					print(array);
				} else {

					currentMax = array[index + 1]; // sonst ist der nachbar das neue maximum
				}
			}
		}

		return array;
	}

	// Hilfsmehtode um das array auszugeben
	public static void print(int[] feld) {
		System.out.print("[");
		for (int wert : feld) {
			System.out.printf(" %d", wert);
		}
		System.out.println(" ]");
	}
}
