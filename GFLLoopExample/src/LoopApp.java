
public class LoopApp {
	public static void main(String[] args) {
		//Teil 2
		double result = 0;
		for (int count = 2; count <= 100; count++) {
			result = result + 1.0 / count;
		}
		System.out.printf("Das ergebnis ist %.4f \n", result);

		//Teil 3
		int sum = 0;
		for (int count = 3; count <= 9000; count = count + 3) {
			sum = sum + count;
			// System.out.printf(" %d->%d\n",count,sum);
		}
		System.out.println(sum);

		//Teil 4
		String matrNr = "47110815";
		sum = 0;
		for (int index = 0; index < matrNr.length(); index++) {
			int temp = Integer.valueOf(""+matrNr.charAt(index));
			sum = sum + temp;
		}
		System.out.printf("MatrNr:%s -> %d\n",matrNr, sum);
	}
}
