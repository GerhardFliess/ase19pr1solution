package at.campus02.ase.reise;

import java.util.ArrayList;

public class Urlaubsreise {

	private String home;
	private ArrayList<Etappe> etappen = new ArrayList<Etappe>();
	private String aktuellerOrt;

	public Urlaubsreise(String home) {
		this.home = home;
	}

	public void addEtappe(String type, int distance, String destination) {

		Etappe temp;
		if (etappen.isEmpty()) {
			temp = new Etappe(type, distance, home, destination);
		} else {
			temp = new Etappe(type, distance, aktuellerOrt, destination);
		}
		etappen.add(temp);

		aktuellerOrt = destination;
	}

	public int ganzeDistanz() {
		int sum = 0;
		for (Etappe etappe : etappen) {
			sum = sum + etappe.getDistance();
		}
		return sum;
	}

	public int zurueckgelegtMit(String type) {
		int sum = 0;
		for (Etappe etappe : etappen) {
			if (etappe.getType() == type)
				sum = sum + etappe.getDistance();
		}
		return sum;
	}

	public String toString() {
		if (etappen.isEmpty())
			return home;
		else
			return etappen.toString();
	}

	public boolean isOneWay() {

		if (etappen.isEmpty())
			return true;

		return aktuellerOrt == home;
	}
}
