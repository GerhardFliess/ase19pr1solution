package at.campus02.ase.reise;

public class Etappe {

	private String type;
	private String end;
	private String start;
	private int distance;

	public Etappe(String type, int distance, String start, String end) {
		this.type = type;
		this.distance = distance;
		this.end = end;
		this.start = start;
	}

	public int getDistance() {
		return distance;
	}

	public String getType() {
		return type;
	}

	public String getEnd() {
		return end;
	}

	public String getStart() {
		return start;
	}

	public String toString() {
		return String.format("%s -> %s (%d, %s)", start, end, distance, type);
	}
}
