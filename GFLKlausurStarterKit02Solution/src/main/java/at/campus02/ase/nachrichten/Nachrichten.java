package at.campus02.ase.nachrichten;

import java.util.ArrayList;

public class Nachrichten {

	String titel;
	String text;
	ArrayList<Nachrichten> nachrichten = new ArrayList<Nachrichten>();

	public Nachrichten(String titel, String text) {
		this.titel = titel;
		this.text = text;
	}

	public Nachrichten antworten(String titel, String text) {

		Nachrichten neueAntwort = new Nachrichten(titel, text);
		nachrichten.add(neueAntwort);
		return neueAntwort;
	}

	public ArrayList<Nachrichten> getAntworten() {
		return nachrichten;
	}

	public int antwortenZaehlen() {
		//anzahl der direkten antworten
		int summeDerAntworten = nachrichten.size();
		
		//in jeder antwort nach deren anzahl nachfragen
		for (Nachrichten aktuelleNachricht : nachrichten) {
			summeDerAntworten = summeDerAntworten
					+ aktuelleNachricht.antwortenZaehlen();
		}

		return summeDerAntworten;
	}

	public String toString() {

		if (nachrichten.size() == 0)
			return String.format("(%s,%s)", titel, text);
		
		return String.format("(%s,%s)%s", titel, text, nachrichten.toString());
	}
}
