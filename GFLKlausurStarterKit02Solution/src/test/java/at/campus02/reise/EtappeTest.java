package at.campus02.reise;

import static org.junit.Assert.*;

import org.junit.Test;

import at.campus02.ase.reise.Etappe;

public class EtappeTest {

	@Test
	public void testConstr01() throws Exception {
		Etappe e = new Etappe("Auto", 200, "Graz", "Wien");
		assertEquals("Auto", e.getType());
		assertEquals("Graz", e.getStart());
		assertEquals("Wien", e.getEnd());
		assertEquals(200, e.getDistance());
	}
	
	@Test
	public void testToString() throws Exception {
		Etappe e = new Etappe("Auto", 200, "Graz", "Wien");
		assertEquals("Auto", e.getType());
		assertEquals("Graz", e.getStart());
		assertEquals("Wien", e.getEnd());
		assertEquals("Graz -> Wien (200, Auto)", e.toString());
	}
}
