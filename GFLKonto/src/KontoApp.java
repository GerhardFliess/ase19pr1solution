
public class KontoApp {

	public static void main(String[] args) {

		double ertraege[] = berechneErtrag(2000, 1.5, 10);
		
		System.out.println(ertraege[9]);
		
	}

	public static double[] berechneErtrag(double kontostand, double zinsen, int laufzeit) {
		
		double feld[] = new double[laufzeit];
		double ergebnis = kontostand;

		for(int count =0; count < laufzeit;count++)
		{
			double ertrag = ergebnis/100*zinsen;
			ergebnis = ergebnis + ertrag;
			feld[count] = ergebnis;
		}
		
		return feld;

	}
}
