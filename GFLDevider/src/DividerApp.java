
public class DividerApp {
	public static void main(String[] args) {

		int zahl = 42;

		int greatestDivider = greatestDivider(zahl);

		System.out.println(greatestDivider);
		System.out.println(greatestDivider(16));
		System.out.println(greatestDivider(17));
		System.out.println(greatestDivider(71));
		System.out.println(greatestDivider(123));

		System.out.println(greatestDivider(1024));

	}

	private static int greatestDivider(int zahl) {
		
		int greatestDivider = 0;
		for (int divider = 2; divider < zahl; divider++) {
			if (zahl % divider == 0)
				greatestDivider = divider;
		}
		
		return greatestDivider;
	}
}
