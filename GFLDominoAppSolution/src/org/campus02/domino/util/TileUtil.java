package org.campus02.domino.util;

import java.util.ArrayList;

import org.campus02.domino.Tile;

public class TileUtil {

	public static int sum(ArrayList<Tile> tiles) {
		int sum = 0;

		for (Tile tile : tiles) {
			sum += tile.getNumber1() + tile.getNumber2();
		}
		return sum;
	}

	public static Tile greatesTile(ArrayList<Tile> tiles) {

		if (tiles == null || tiles.isEmpty())
			return null;

		Tile max = tiles.get(0);
		for (Tile aktuellesTile : tiles) {
			if (max.compare(aktuellesTile))
				max = aktuellesTile;
		}

		return max;
	}
}
