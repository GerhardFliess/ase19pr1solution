package org.campus02.set;

import java.util.ArrayList;

public class Set {

	private ArrayList<Integer> values = new ArrayList<Integer>();
	private ArrayList<Set> sets = new ArrayList<Set>();

	public void add(int value) {
		values.add(value);
	}

	public void add(Set set) {

		sets.add(set);
	}

	public int sum() {

		int result = 0;
		// summe der eigenen zahlen
		for (Integer integer : values) {
			result += integer;
		}
		// summe der zahlen in den eingefügten sets
		for (Set set : sets) {
			result += set.sum();
		}

		return result;
	}

	@Override
	public String toString() {
		String result = "";
		for (Integer integer : values) {
			result = result + "," + integer;
		}
		// wenn es keine eingefügten sets gibt
		if (sets.isEmpty()) {
			return "{" + result.substring(1) + "}";
		} else {

			// Bei eingefügten sets zum string auch noch diese hinzufügen
			String setsToString = "";
			for (Set set : sets) {
				setsToString += set.toString();
			}
			return "{" + result.substring(1) + setsToString + "}";

		}

	}

}
