package edu.campus02.dosen;

import java.util.ArrayList;

public class Keksdose {

	private double breite;
	private double hoehe;
	private double laenge;
	private String farbe;
	private Keksdose gestapelteDose;

	private ArrayList<Keks> kekse = new ArrayList<Keks>();

	public Keksdose(String farbe, double x, double y, double hoehe) {

		if (x > y) {
			this.breite = y;
			this.laenge = x;
		} else {
			this.breite = x;
			this.laenge = y;
		}
		this.hoehe = hoehe;
		this.farbe = farbe;
	}

	public double getBreite() {
		return breite;
	}

	public double getLaenge() {
		return laenge;
	}

	public double getHoehe() {
		return hoehe;
	}

	public double volumen() {
		return breite * hoehe * laenge;
	}

	public double freiesVolumen() {
		double sumKekse = 0;
		for (Keks keks : kekse) {
			sumKekse += keks.getVolumen();
		}
		return volumen() - sumKekse;
	}

	public boolean keksAblegen(Keks keks) {

		if (freiesVolumen() >= keks.getVolumen()) {
			kekse.add(keks);
			return true;
		}
		return false;
	}

	public boolean leer() {
		return kekse.isEmpty();
	}

	public boolean stapeln(Keksdose d) {
		if (!leer())
			return false;

		if (breite <= d.breite)
			return false;
		if (laenge <= d.laenge)
			return false;
		if (hoehe <= d.hoehe)
			return false;

		if (gestapelteDose == null) {
			gestapelteDose = d;
			return true;

		} else {
			return gestapelteDose.stapeln(d);
		}
	}

	public double gesamtVolumen() {
		double result = volumen();
		if (gestapelteDose != null)
			result = result + gestapelteDose.gesamtVolumen();
		return result;
	}

	public String toString() {
		if (gestapelteDose == null)
			return String.format("(%s %.1f x %.1f x %.1f)", farbe, breite, laenge, hoehe);
		else
			return String.format("(%s %.1f x %.1f x %.1f %s)", farbe, breite, laenge, hoehe, gestapelteDose.toString());

	}
}
