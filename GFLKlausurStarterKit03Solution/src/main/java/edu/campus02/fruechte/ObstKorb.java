package edu.campus02.fruechte;

import java.util.ArrayList;
import java.util.HashSet;

public class ObstKorb {

	private ArrayList<Frucht> fruechte = new ArrayList<Frucht>();
	public void fruchtAblegen(Frucht f) {
		fruechte.add(f);
	}

	public int zaehleFruechte(String sorte) {

		int anzahl = 0;
		for (Frucht frucht : fruechte) {
			if (frucht.getSorte() == sorte)
				anzahl++;
		}
		return anzahl;
	}

	public Frucht schwersteFrucht() {
		if (fruechte.isEmpty())
			return null;

		Frucht max = fruechte.get(0);
		for (Frucht temp : fruechte) {
			if (temp.getGewicht() > max.getGewicht())
				max = temp;
		}

		return max;
	}

	public ArrayList<Frucht> fruechteSchwererAls(double gewicht) {
		ArrayList<Frucht> result = new ArrayList<Frucht>();
		for (Frucht temp : fruechte) {
			if (temp.getGewicht() > gewicht)
				result.add(temp);
		}
		return result;
	}

	public int zaehleSorte() {
		HashSet<String> sorten = new HashSet<String>();
		for (Frucht frucht : fruechte) {
			sorten.add(frucht.getSorte());
		}
		return sorten.size();
	}

	public String toString() {
		return fruechte.toString();
	}
}
