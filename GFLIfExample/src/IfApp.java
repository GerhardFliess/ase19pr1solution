
public class IfApp {

	public static void main(String[] args) {
		int test = -6;

		if (test < 0) {
			System.out.printf(" %d ist kleiner als 0", test);
		} else if (test > 0) {
			System.out.printf(" %d ist gr��er als 0", test);
		} else {
			System.out.printf(" %d ist  0", test);
		}
		
		System.out.println();
		if (test % 3 == 0) {
			System.out.printf(" %d l�sst sich durch 3 teilen", test);
		} else {
			System.out.printf(" %d l�sst sich nicht durch 3 teilen", test);
		}

	}

}
