
public class IfAppMethod {

	public static void main(String[] args) {
		zeroTest(3);
		zeroTest(-5);
		zeroTest(6);
		zeroTest(-9);

		moduloTest(3, 3);
		moduloTest(-5, 3);
		moduloTest(6, 3);
		moduloTest(-9, 3);

	}

	private static void moduloTest(int test, int div) {
		if (test % div == 0) {
			System.out.printf(" %d l�sst sich durch %d teilen \n", test, div);
		} else {
			System.out.printf(" %d l�sst sich nicht durch %d teilen\n", test, div);
		}
	}

	private static void zeroTest(int test) {
		if (test < 0) {
			System.out.printf(" %d ist kleiner als 0\n", test);
		} else if (test > 0) {
			System.out.printf(" %d ist gr��er als 0\n", test);
		} else {
			System.out.printf(" %d ist  0", test);
		}
	}

}
